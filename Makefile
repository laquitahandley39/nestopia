SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

NAME := nestopia
JGNAME := $(NAME)-jg

FLAGS := -std=c++98
DEFS := -DNST_PRAGMA_ONCE -DNST_NO_ZLIB

SRCDIR := $(SOURCEDIR)/src

LIBS = -lm -lstdc++

LIBS_REQUIRES :=

DOCS := COPYING README

# Object dirs
MKDIRS := src/api src/board src/input src/vssystem

override INSTALL_DATA := 1
override INSTALL_EXAMPLE := 0
override INSTALL_SHARED := 0

include $(SOURCEDIR)/version.h
include $(SOURCEDIR)/mk/jg.mk

EXT := cpp
LINKER := $(CXX)

# Core
CXXSRCS := src/NstApu.cpp \
	src/NstAssert.cpp \
	src/NstCartridge.cpp \
	src/NstCartridgeInes.cpp \
	src/NstCartridgeRomset.cpp \
	src/NstCartridgeUnif.cpp \
	src/NstCheats.cpp \
	src/NstChecksum.cpp \
	src/NstChips.cpp \
	src/NstCore.cpp \
	src/NstCpu.cpp \
	src/NstCrc32.cpp \
	src/NstFds.cpp \
	src/NstFile.cpp \
	src/NstHomebrew.cpp \
	src/NstImage.cpp \
	src/NstImageDatabase.cpp \
	src/NstLog.cpp \
	src/NstMachine.cpp \
	src/NstMemory.cpp \
	src/NstNsf.cpp \
	src/NstPatcher.cpp \
	src/NstPatcherIps.cpp \
	src/NstPatcherUps.cpp \
	src/NstPins.cpp \
	src/NstPpu.cpp \
	src/NstProperties.cpp \
	src/NstRam.cpp \
	src/NstSha1.cpp \
	src/NstSoundPcm.cpp \
	src/NstSoundPlayer.cpp \
	src/NstSoundRenderer.cpp \
	src/NstState.cpp \
	src/NstStream.cpp \
	src/NstTracker.cpp \
	src/NstTrackerMovie.cpp \
	src/NstTrackerRewinder.cpp \
	src/NstVector.cpp \
	src/NstVideoFilterNone.cpp \
	src/NstVideoFilterNtsc.cpp \
	src/NstVideoFilterNtscCfg.cpp \
	src/NstVideoRenderer.cpp \
	src/NstVideoScreen.cpp \
	src/NstXml.cpp \
	src/NstZlib.cpp \
	src/api/NstApiBarcodeReader.cpp \
	src/api/NstApiCartridge.cpp \
	src/api/NstApiCheats.cpp \
	src/api/NstApiDipSwitches.cpp \
	src/api/NstApiEmulator.cpp \
	src/api/NstApiFds.cpp \
	src/api/NstApiHomebrew.cpp \
	src/api/NstApiInput.cpp \
	src/api/NstApiMachine.cpp \
	src/api/NstApiMovie.cpp \
	src/api/NstApiNsf.cpp \
	src/api/NstApiRewinder.cpp \
	src/api/NstApiSound.cpp \
	src/api/NstApiTapeRecorder.cpp \
	src/api/NstApiUser.cpp \
	src/api/NstApiVideo.cpp \
	src/board/NstBoardAcclaimMcAcc.cpp \
	src/board/NstBoardAction53.cpp \
	src/board/NstBoardAe.cpp \
	src/board/NstBoardAgci.cpp \
	src/board/NstBoardAveD1012.cpp \
	src/board/NstBoardAveNina.cpp \
	src/board/NstBoardAxRom.cpp \
	src/board/NstBoardBandai24c0x.cpp \
	src/board/NstBoardBandaiAerobicsStudio.cpp \
	src/board/NstBoardBandaiDatach.cpp \
	src/board/NstBoardBandaiKaraokeStudio.cpp \
	src/board/NstBoardBandaiLz93d50.cpp \
	src/board/NstBoardBandaiLz93d50ex.cpp \
	src/board/NstBoardBandaiOekaKids.cpp \
	src/board/NstBoardBenshengBs5.cpp \
	src/board/NstBoardBmc110in1.cpp \
	src/board/NstBoardBmc1200in1.cpp \
	src/board/NstBoardBmc120in1.cpp \
	src/board/NstBoardBmc150in1.cpp \
	src/board/NstBoardBmc15in1.cpp \
	src/board/NstBoardBmc20in1.cpp \
	src/board/NstBoardBmc21in1.cpp \
	src/board/NstBoardBmc22Games.cpp \
	src/board/NstBoardBmc31in1.cpp \
	src/board/NstBoardBmc35in1.cpp \
	src/board/NstBoardBmc36in1.cpp \
	src/board/NstBoardBmc64in1.cpp \
	src/board/NstBoardBmc72in1.cpp \
	src/board/NstBoardBmc76in1.cpp \
	src/board/NstBoardBmc800in1.cpp \
	src/board/NstBoardBmc8157.cpp \
	src/board/NstBoardBmc9999999in1.cpp \
	src/board/NstBoardBmcA65as.cpp \
	src/board/NstBoardBmcBallgames11in1.cpp \
	src/board/NstBoardBmcCh001.cpp \
	src/board/NstBoardBmcCtc65.cpp \
	src/board/NstBoardBmcFamily4646B.cpp \
	src/board/NstBoardBmcFk23c.cpp \
	src/board/NstBoardBmcGamestarA.cpp \
	src/board/NstBoardBmcGamestarB.cpp \
	src/board/NstBoardBmcGolden190in1.cpp \
	src/board/NstBoardBmcGoldenCard6in1.cpp \
	src/board/NstBoardBmcGoldenGame260in1.cpp \
	src/board/NstBoardBmcHero.cpp \
	src/board/NstBoardBmcMarioParty7in1.cpp \
	src/board/NstBoardBmcNovelDiamond.cpp \
	src/board/NstBoardBmcPowerjoy84in1.cpp \
	src/board/NstBoardBmcResetBased4in1.cpp \
	src/board/NstBoardBmcSuper22Games.cpp \
	src/board/NstBoardBmcSuper24in1.cpp \
	src/board/NstBoardBmcSuper40in1.cpp \
	src/board/NstBoardBmcSuper700in1.cpp \
	src/board/NstBoardBmcSuperBig7in1.cpp \
	src/board/NstBoardBmcSuperGun20in1.cpp \
	src/board/NstBoardBmcSuperHiK300in1.cpp \
	src/board/NstBoardBmcSuperHiK4in1.cpp \
	src/board/NstBoardBmcSuperVision16in1.cpp \
	src/board/NstBoardBmcT262.cpp \
	src/board/NstBoardBmcVrc4.cpp \
	src/board/NstBoardBmcVt5201.cpp \
	src/board/NstBoardBmcY2k64in1.cpp \
	src/board/NstBoardBtl2708.cpp \
	src/board/NstBoardBtl6035052.cpp \
	src/board/NstBoardBtlAx5705.cpp \
	src/board/NstBoardBtlDragonNinja.cpp \
	src/board/NstBoardBtlGeniusMerioBros.cpp \
	src/board/NstBoardBtlMarioBaby.cpp \
	src/board/NstBoardBtlPikachuY2k.cpp \
	src/board/NstBoardBtlShuiGuanPipe.cpp \
	src/board/NstBoardBtlSmb2a.cpp \
	src/board/NstBoardBtlSmb2b.cpp \
	src/board/NstBoardBtlSmb2c.cpp \
	src/board/NstBoardBtlSmb3.cpp \
	src/board/NstBoardBtlSuperBros11.cpp \
	src/board/NstBoardBtlT230.cpp \
	src/board/NstBoardBtlTobidaseDaisakusen.cpp \
	src/board/NstBoardBxRom.cpp \
	src/board/NstBoardCaltron.cpp \
	src/board/NstBoardCamerica.cpp \
	src/board/NstBoardCneDecathlon.cpp \
	src/board/NstBoardCnePsb.cpp \
	src/board/NstBoardCneShlz.cpp \
	src/board/NstBoardCony.cpp \
	src/board/NstBoard.cpp \
	src/board/NstBoardCxRom.cpp \
	src/board/NstBoardDiscrete.cpp \
	src/board/NstBoardDreamtech.cpp \
	src/board/NstBoardEvent.cpp \
	src/board/NstBoardFb.cpp \
	src/board/NstBoardFfe.cpp \
	src/board/NstBoardFujiya.cpp \
	src/board/NstBoardFukutake.cpp \
	src/board/NstBoardFutureMedia.cpp \
	src/board/NstBoardGouder.cpp \
	src/board/NstBoardGxRom.cpp \
	src/board/NstBoardHenggedianzi.cpp \
	src/board/NstBoardHes.cpp \
	src/board/NstBoardHosenkan.cpp \
	src/board/NstBoardInlNsf.cpp \
	src/board/NstBoardIremG101.cpp \
	src/board/NstBoardIremH3001.cpp \
	src/board/NstBoardIremHolyDiver.cpp \
	src/board/NstBoardIremKaiketsu.cpp \
	src/board/NstBoardIremLrog017.cpp \
	src/board/NstBoardJalecoJf11.cpp \
	src/board/NstBoardJalecoJf13.cpp \
	src/board/NstBoardJalecoJf16.cpp \
	src/board/NstBoardJalecoJf17.cpp \
	src/board/NstBoardJalecoJf19.cpp \
	src/board/NstBoardJalecoSs88006.cpp \
	src/board/NstBoardJyCompany.cpp \
	src/board/NstBoardKaiser.cpp \
	src/board/NstBoardKasing.cpp \
	src/board/NstBoardKayH2288.cpp \
	src/board/NstBoardKayPandaPrince.cpp \
	src/board/NstBoardKonamiVrc1.cpp \
	src/board/NstBoardKonamiVrc2.cpp \
	src/board/NstBoardKonamiVrc3.cpp \
	src/board/NstBoardKonamiVrc4.cpp \
	src/board/NstBoardKonamiVrc6.cpp \
	src/board/NstBoardKonamiVrc7.cpp \
	src/board/NstBoardKonamiVsSystem.cpp \
	src/board/NstBoardMagicKidGoogoo.cpp \
	src/board/NstBoardMagicSeries.cpp \
	src/board/NstBoardMmc1.cpp \
	src/board/NstBoardMmc2.cpp \
	src/board/NstBoardMmc3.cpp \
	src/board/NstBoardMmc4.cpp \
	src/board/NstBoardMmc5.cpp \
	src/board/NstBoardMmc6.cpp \
	src/board/NstBoardNamcot163.cpp \
	src/board/NstBoardNamcot175.cpp \
	src/board/NstBoardNamcot340.cpp \
	src/board/NstBoardNamcot34xx.cpp \
	src/board/NstBoardNanjing.cpp \
	src/board/NstBoardNihon.cpp \
	src/board/NstBoardNitra.cpp \
	src/board/NstBoardNtdec.cpp \
	src/board/NstBoardOpenCorp.cpp \
	src/board/NstBoardQj.cpp \
	src/board/NstBoardRcm.cpp \
	src/board/NstBoardRexSoftDb5z.cpp \
	src/board/NstBoardRexSoftSl1632.cpp \
	src/board/NstBoardRumbleStation.cpp \
	src/board/NstBoardSachen74x374.cpp \
	src/board/NstBoardSachenS8259.cpp \
	src/board/NstBoardSachenSa0036.cpp \
	src/board/NstBoardSachenSa0037.cpp \
	src/board/NstBoardSachenSa72007.cpp \
	src/board/NstBoardSachenSa72008.cpp \
	src/board/NstBoardSachenStreetHeroes.cpp \
	src/board/NstBoardSachenTca01.cpp \
	src/board/NstBoardSachenTcu.cpp \
	src/board/NstBoardSomeriTeamSl12.cpp \
	src/board/NstBoardSubor.cpp \
	src/board/NstBoardSunsoft1.cpp \
	src/board/NstBoardSunsoft2.cpp \
	src/board/NstBoardSunsoft3.cpp \
	src/board/NstBoardSunsoft4.cpp \
	src/board/NstBoardSunsoft5b.cpp \
	src/board/NstBoardSunsoftDcs.cpp \
	src/board/NstBoardSunsoftFme7.cpp \
	src/board/NstBoardSuperGameBoogerman.cpp \
	src/board/NstBoardSuperGameLionKing.cpp \
	src/board/NstBoardSuperGamePocahontas2.cpp \
	src/board/NstBoardTaitoTc0190fmc.cpp \
	src/board/NstBoardTaitoTc0190fmcPal16r4.cpp \
	src/board/NstBoardTaitoX1005.cpp \
	src/board/NstBoardTaitoX1017.cpp \
	src/board/NstBoardTengen.cpp \
	src/board/NstBoardTengenRambo1.cpp \
	src/board/NstBoardTxc.cpp \
	src/board/NstBoardTxcMxmdhtwo.cpp \
	src/board/NstBoardTxcPoliceman.cpp \
	src/board/NstBoardTxcTw.cpp \
	src/board/NstBoardTxRom.cpp \
	src/board/NstBoardUnl158b.cpp \
	src/board/NstBoardUnlA9746.cpp \
	src/board/NstBoardUnlCc21.cpp \
	src/board/NstBoardUnlEdu2000.cpp \
	src/board/NstBoardUnlFam250Schi24.cpp \
	src/board/NstBoardUnlKingOfFighters96.cpp \
	src/board/NstBoardUnlKingOfFighters97.cpp \
	src/board/NstBoardUnlMmc3BigPrgRom.cpp \
	src/board/NstBoardUnlMortalKombat2.cpp \
	src/board/NstBoardUnlN625092.cpp \
	src/board/NstBoardUnlRetX7Gbl.cpp \
	src/board/NstBoardUnlSuperFighter3.cpp \
	src/board/NstBoardUnlTf1201.cpp \
	src/board/NstBoardUnlWorldHero.cpp \
	src/board/NstBoardUnlXzy.cpp \
	src/board/NstBoardUxRom.cpp \
	src/board/NstBoardVsSystem.cpp \
	src/board/NstBoardWaixing.cpp \
	src/board/NstBoardWaixingFfv.cpp \
	src/board/NstBoardWaixingFs304.cpp \
	src/board/NstBoardWaixingPs2.cpp \
	src/board/NstBoardWaixingSecurity.cpp \
	src/board/NstBoardWaixingSgz.cpp \
	src/board/NstBoardWaixingSgzlz.cpp \
	src/board/NstBoardWaixingSh2.cpp \
	src/board/NstBoardWaixingZs.cpp \
	src/board/NstBoardWhirlwind.cpp \
	src/board/NstBoardZz.cpp \
	src/input/NstInpAdapter.cpp \
	src/input/NstInpBandaiHyperShot.cpp \
	src/input/NstInpBarcodeWorld.cpp \
	src/input/NstInpCrazyClimber.cpp \
	src/input/NstInpDoremikkoKeyboard.cpp \
	src/input/NstInpExcitingBoxing.cpp \
	src/input/NstInpFamilyKeyboard.cpp \
	src/input/NstInpFamilyTrainer.cpp \
	src/input/NstInpHoriTrack.cpp \
	src/input/NstInpKonamiHyperShot.cpp \
	src/input/NstInpMahjong.cpp \
	src/input/NstInpMouse.cpp \
	src/input/NstInpOekaKidsTablet.cpp \
	src/input/NstInpPachinko.cpp \
	src/input/NstInpPad.cpp \
	src/input/NstInpPaddle.cpp \
	src/input/NstInpPartyTap.cpp \
	src/input/NstInpPokkunMoguraa.cpp \
	src/input/NstInpPowerGlove.cpp \
	src/input/NstInpPowerPad.cpp \
	src/input/NstInpRob.cpp \
	src/input/NstInpSuborKeyboard.cpp \
	src/input/NstInpTopRider.cpp \
	src/input/NstInpTurboFile.cpp \
	src/input/NstInpZapper.cpp \
	src/vssystem/NstVsRbiBaseball.cpp \
	src/vssystem/NstVsSuperXevious.cpp \
	src/vssystem/NstVsSystem.cpp \
	src/vssystem/NstVsTkoBoxing.cpp

JGSRCS := jg.cpp

# Assets
ROMDB := NstDatabase.xml
PALETTES := palettes/SONY_CXA2025AS_US.pal palettes/Royaltea.pal \
	palettes/Nobilitea.pal palettes/Digital_Prime_FBX.pal \
	palettes/Magnum_FBX.pal palettes/PVM_Style_D93_FBX.pal \
	palettes/Smooth_V2_FBX.pal

PALETTES_BASE := $(notdir $(PALETTES))

ROMDB_TARGET := $(ROMDB:%=$(NAME)/%)
PALETTES_TARGET := $(PALETTES_BASE:%=$(NAME)/%)

DATA := $(ROMDB) $(PALETTES_BASE)
DATA_TARGET := $(ROMDB_TARGET) $(PALETTES_TARGET)

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CXXSRCS:.cpp=.o))
OBJS_JG := $(patsubst %,$(OBJDIR)/%,$(JGSRCS:.cpp=.o))

# Core commands
BUILD_JG = $(call COMPILE_CXX, $(FLAGS) $(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_CXX, $(FLAGS) $(DEFS))

.PHONY: $(PHONY)

all: $(TARGET)

# Core rules
$(OBJDIR)/src/%.o: $(SRCDIR)/%.$(EXT) $(PREREQ)
	$(call COMPILE_INFO,$(BUILD_MAIN))
	@$(BUILD_MAIN)

# Data rules
$(ROMDB_TARGET): $(ROMDB:%=$(SOURCEDIR)/%)
	@mkdir -p $(NAME)
	@cp $(subst $(NAME),$(SOURCEDIR)/,$@) $(NAME)/

$(PALETTES_TARGET): $(PALETTES:%=$(SOURCEDIR)/%)
	@mkdir -p $(NAME)/palettes
	@cp $(subst $(NAME),$(SOURCEDIR)/palettes,$@) $(NAME)/palettes/

install-data: all
	@mkdir -p $(DESTDIR)$(DATADIR)/jollygood/$(NAME)
	cp $(NAME)/NstDatabase.xml $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp -r $(NAME)/palettes $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/

include $(SOURCEDIR)/mk/rules.mk
